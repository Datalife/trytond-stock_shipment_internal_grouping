# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelView, ModelSQL, Workflow
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, If, In, Bool, Not, Equal, Id
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateTransition, StateView, Button

__all__ = ['ShipmentInternalGroup', 'AssignShipmentInternalGroup',
           'AssignShipmentInternalGroupAssignFailed',
           'ShipmentInternal', 'Configuration']

__metaclass__ = PoolMeta


class ShipmentInternalGroup(Workflow, ModelView, ModelSQL):
    """Internal Shipment Group"""
    __name__ = 'stock.shipment.internal.group'
    _rec_name = 'code'

    code = fields.Char('Code', select=True, readonly=True)
    reference = fields.Char('Reference', select=True,
                            states={'readonly': Not(Equal(Eval('state'), 'draft'))},
                            depends=['state'])
    company = fields.Many2One('company.company', 'Company', required=True,
                              states={'readonly': Bool(Eval('shipments'))},
                              domain=[('id', If(In('company', Eval('context', {})), '=', '!='),
                                       Eval('context', {}).get('company', -1))],
                              depends=['shipments'])
    shipments = fields.One2Many('stock.shipment.internal', 'group', 'Shipments',
                                domain=[('company', '=', Eval('company'))],
                                states={'readonly': Bool(True)})
    state = fields.Selection([('draft', 'Draft'),
                              ('cancelled', 'Cancelled'),
                              ('assigned', 'Assigned'),
                              ('waiting', 'Waiting'),
                              ('done', 'Done')], 'State', readonly=True)

    @classmethod
    def __setup__(cls):
        super(ShipmentInternalGroup, cls).__setup__()
        cls._order[0] = ('id', 'DESC')
        cls._error_messages.update({
                'delete_cancel': ('Internal Shipment Group "%s" must be cancelled '
                    'before deletion.'),
                })
        cls._transitions |= set((
                ('draft', 'waiting'),
                ('waiting', 'waiting'),
                ('waiting', 'assigned'),
                ('assigned', 'done'),
                ('waiting', 'draft'),
                ('assigned', 'waiting'),
                ('draft', 'cancelled'),
                ('waiting', 'cancelled'),
                ('assigned', 'cancelled'),
                ('cancelled', 'draft'),
                ))
        cls._buttons.update({
                'cancel': {'invisible': Eval('state').in_(['cancelled', 'done'])},
                'draft': {'invisible': ~Eval('state').in_(['cancelled', 'waiting']),
                          'icon': If(Eval('state') == 'cancelled', 'tryton-clear', 'tryton-go-previous')},
                'wait': {'invisible': ~Eval('state').in_(['assigned', 'waiting', 'draft']),
                         'icon': If(Eval('state') == 'assigned', 'tryton-go-previous',
                                    If(Eval('state') == 'waiting', 'tryton-clear', 'tryton-go-next'))},
                'done': {'invisible': Eval('state') != 'assigned'},
                'assign_wizard': {'invisible': Eval('state') != 'waiting',
                                   'readonly': ~Eval('groups', []).contains(Id('stock', 'group_stock'))},
                'assign_try': {},
                'assign_force': {},
                })

    @classmethod
    def __register__(cls, module_name):
        cursor = Transaction().connection.cursor()
        sql_table = cls.__table__()

        super().__register__(module_name)

        # Migration from 5.6: rename state cancel to cancelled
        cursor.execute(*sql_table.update(
                [sql_table.state], ['cancelled'],
                where=sql_table.state == 'cancel'))

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('stock.configuration')

        vlist = [x.copy() for x in vlist]
        config = Config(1)
        for values in vlist:
            values['code'] = Sequence.get_id(config.shipment_internal_group_sequence.id)
        return super(ShipmentInternalGroup, cls).create(vlist)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, groups):
        Shipment = Pool().get('stock.shipment.internal')
        Shipment.draft([m for g in groups for m in g.shipments])

    @classmethod
    @ModelView.button
    @Workflow.transition('waiting')
    def wait(cls, groups):
        Shipment = Pool().get('stock.shipment.internal')
        Shipment.wait([m for g in groups for m in g.shipments])

    @classmethod
    @Workflow.transition('assigned')
    def assign(cls, groups):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, groups):
        Shipment = Pool().get('stock.shipment.internal')
        Shipment.done([m for g in groups for m in g.shipments])

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, groups):
        Shipment = Pool().get('stock.shipment.internal')
        Shipment.cancel([m for g in groups for m in g.shipments])

    @classmethod
    @ModelView.button_action('stock_shipment_internal_grouping.wizard_shipment_internal_group_assign')
    def assign_wizard(cls, groups):
        pass

    @classmethod
    @ModelView.button
    def assign_try(cls, groups):
        Shipment = Pool().get('stock.shipment.internal')
        if Shipment.assign_try([m for g in groups for m in g.shipments]):
            cls.assign(groups)
            return True
        else:
            return False

    @classmethod
    @ModelView.button
    def assign_force(cls, groups):
        Shipment = Pool().get('stock.shipment.internal')
        Shipment.assign_force([m for g in groups for m in g.shipments])
        cls.assign(groups)


class AssignShipmentInternalGroupAssignFailed(ModelView):
    """Assign Shipment Internal Group"""
    __name__ = 'stock.shipment.internal.group.assign.failed'

    moves = fields.Many2Many('stock.move', None, None, 'Moves', readonly=True)

    @staticmethod
    def default_moves():
        ShipmentInternalGroup = Pool().get('stock.shipment.internal.group')
        group_id = Transaction().context.get('active_id')
        if not group_id:
            return []
        group = ShipmentInternalGroup(group_id)
        return [m.id for s in group.shipments for m in s.moves if m.state == 'draft']


class AssignShipmentInternalGroup(Wizard):
    """Assign Shipment Internal Group"""
    __name__ = 'stock.shipment.internal.group.assign'

    start = StateTransition()
    failed = StateView('stock.shipment.internal.group.assign.failed',
                       'stock_shipment_internal_grouping.shipment_internal_group_assign_failed_view_form',
                       [Button('Force Assign', 'force', 'tryton-go-next',
                               states={'invisible': ~Id('stock', 'group_stock_force_assignment').in_(
                                   Eval('context', {}).get('groups', []))}),
                        Button('Ok', 'end', 'tryton-ok', True)])
    force = StateTransition()

    def transition_start(self):
        pool = Pool()
        ShipmentGroup = pool.get('stock.shipment.internal.group')

        if ShipmentGroup.assign_try([ShipmentGroup(Transaction().context['active_id'])]):
            return 'end'
        else:
            return 'failed'

    def transition_force(self):
        ShipmentGroup = Pool().get('stock.shipment.internal.group')

        ShipmentGroup.assign_force([ShipmentGroup(Transaction().context['active_id'])])
        return 'end'


class ShipmentInternal:
    __name__ = 'stock.shipment.internal'

    group = fields.Many2One('stock.shipment.internal.group', 'Group',
                            ondelete='CASCADE', select=True,
                            domain=[('company', '=', Eval('company'))],
                            states={'readonly': Not(Equal(Eval('state'), 'draft'))},
                            depends=['state'])


class Configuration:
    __name__ = 'stock.configuration'

    shipment_internal_group_sequence = fields.Property(
        fields.Many2One('ir.sequence',
                        'Internal Shipment Group Sequence',
                        domain=[('code', '=', 'stock.shipment.internal.group'),
                                ('name', '=', 'Internal Shipment Group')],
                        required=True))
