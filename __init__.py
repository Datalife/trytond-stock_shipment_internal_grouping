from trytond.pool import Pool
from .shipment import *


def register():
    Pool.register(
        ShipmentInternalGroup,
        AssignShipmentInternalGroupAssignFailed,
        ShipmentInternal,
        Configuration,
        module='stock_shipment_internal_grouping', type_='model')
    Pool.register(
        AssignShipmentInternalGroup,
        module='stock_shipment_internal_grouping', type_='wizard')